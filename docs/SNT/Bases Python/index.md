---
hide:
    - toc
---

C'est la première partie de l'année, il s'agit d'une initiation à la programmation. Nous nous concentrons particulièrement sur l'algorithmique (des programmes en français) et sur le langage Python que l'on retrouve dans les programmes de Mathématiques, Sciences Physiques et Sciences de la vie et de la terre de la classe de seconde.

Les notebooks jupyter sont mis à disposition sur classroom.

#### Variables et premières instructions &nbsp;&nbsp; [Cours](cours1.md) &nbsp;&nbsp;&nbsp;&nbsp; [Exercices](exos1.md)

#### Structure alternative &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Cours](cours2.md) &nbsp;&nbsp;&nbsp;&nbsp; [Exercices](exos2.md)

#### Structure itérative &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Cours](cours3.md) &nbsp;&nbsp;&nbsp;&nbsp; [Exercices](exos3.md)