---
date: September 15, 2022
geometry: "left=2cm,right=2cm,top=1.3cm,bottom=1.3cm"
output: pdf_document
---

# Structure Itérative

## L’itération

**Définition** 

Une itération est caractérisée :

- par une séquence d’opérations à répéter (chaque exécution s’appelle une boucle)

- par une condition logique indiquant quand l’itération doit se terminer.

La syntaxe peut prendre 3 formes différentes :	

- TANTQUE ... FINTANTQUE
- REPETER ... JUSQU'A
- POUR ... FINPOUR

## La syntaxe TantQue… FinTantQue  :

Cette instruction permet de répéter la structure itérative tant que la condition logique est vérifiée.

**Principe:**

```
TANTQUE Condition
	.
	Instructions
	.
FINTANTQUE
```
**Exemple:**

```
TANTQUE je n'ai pas la moyenne
	Je travaille 2h tous les soirs
FINTANTQUE
```

## La syntaxe Répeter … Jusqu’à  :

**Principe**
```		
REPETER
	Instructions
JUSQU'A Condition
```	
**Exemple**

```		
REPETER
	Avancer d'un pas
JUSQU'A je suis au bord du ravin
```
*Question*

Y-a-t-il un risque de tomber ?

## La syntaxe Pour… FinPour

On utilise cette syntaxe pour une situation qui se répète un nombre de fois connu et fixé.

**Principe**

```		
POUR i allant de 1 à N
	Instructions
FINPOUR Condition
```

*Exemples*

```
POUR i allant de 1 à 5
	Afficher "Bonjour !"
FINPOUR
```

On peut aussi utiliser la variable i dans la boucle

```
POUR i allant de 1 à 5
	Afficher i*3
FINPOUR
```

## Synthèse 

Le choix d’une structure itérative dépend de 2 critères décrit dans cette structure alternative :

```
    Connaît-on le nombre d’exécution de la boucle ?
	Si oui Alors 
        Structure POUR … FINPOUR
	Sinon
        Le nombre d’exécution de la boucle peut-il être nul (égal à 0) ?
		Si oui Alors
		    Structure TANTQUE … FINTANTQUE
        Sinon
            Structure REPETER … JUSQU'A
        Finsi
	Finsi
```

## En plus...

- Dans une structure itérative, on fait attention à ne pas modifier dans la boucle la variable sur laquelle porte la condition, on risquerait alors une boucle infinie.

- Dans l'instruction « POUR FINPOUR » la variable i est appelée variable muette, elle peut aussi être modifiée dans la boucle !

