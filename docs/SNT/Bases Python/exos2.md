---
date: September 5, 2022
geometry: "left=2cm,right=2cm,top=1.3cm,bottom=1.3cm"
output: pdf_document
---
#
## Exercice 1 - TVA et exportation

La société IGS vend des logiciels de jeux en Europe et en Amérique du Nord. Elle désirerait automatiser le calcul du net à payer de ses factures en fonction du net commercial.
Au net commercial, s’ajoute les frais de transport (10€ HT forfaitaire uniquement pour les exportations). La TVA(20%)  ne s’applique qu’aux clients français.

<u>**Travail à faire :**</u> Compléter l’algorithme ci-dessous.

**Variables :**
```
RS :	Chaîne (Nom du client)
PA   :	Chaîne (Pays client)
NC : 	Réel (Net commercial HT)
NP :	Réel (Net a payer)

Constante :
TVA ← 20%
```
**Début** 
```
Saisir « Raison sociale du client » , RS
Saisir « Pays du client » , PA
Saisir « Net commercial » , NC





Afficher « Le montant à payer est de : » , NP
```
**Fin**

## Exercice 2 - Réduction financière 

La société OME décide d’accorder à ses clients un escompte de 2% si le net commercial de la facture est supérieur à 2000€ HT et si le paiement est effectué comptant.

<u>**Travail à faire : **</u> Réaliser l’algorithme permettant après la saisie du net commercial et des conditions de règlement, de calculer le net à payer (TVA 20%)

## Exercice 3 - Frais de transport (Sujet 2ème groupe 1998)

TOUTNET SA utilise les services d'un transporteur pour assurer les livraisons des commandes auprès de ses clients. Celui-ci propose deux types de prestations :

- une prestation "transport URGENT" pour livraison dans le département 54 uniquement. Le montant HT est calculé en appliquant les règles suivantes:

    - si le poids urgent est inférieur ou égal à 100 kg: le tarif 1 sera appliqué sur ce poids.
    - si le poids urgent est inférieur ou égal à 1000 kg: le tarif 1 sera appliqué jusqu'à 100 kg , puis sur la tranche restante, on appliquera le tarif 2.
    - à partir de 1 001 kg, on continuera à appliquer un système de tranche avec, sur la partie supérieure à 1000 kg, le tarif 3.

Tarif 1 : 1.5€ par Kg

Tarif 2 : 1.00€ par Kg

Tarif 3 : 0.50€ par Kg

- une prestation "transport NORMAL" dont le tarif est déterminé en fonction du poids et du département de destination.


<u>**Travail à faire :**</u> Présentez l'algorithme correspondant au calcul du montant HT d'une prestation de "transport URGENT".


## Exercice 4 - La société « Skiplus »

La société « Skiplus » fabrique et distribue 2 types de surf des neiges :
- Le modèle « XL100 », haut de gamme, est vendu au prix moyen de 350€ HT.
- Le modèle « XS50 », grande diffusion, est vendu au prix moyen de 180€ HT.

Huit représentants sont chargés de la commercialisation de ces deux produits. Leur rémunération se calcule ainsi :
- Un fixe de 1100€, majoré de 5% à partir de 5 ans d’ancienneté ou de 10% au-delà  de 10 ans. 
- Une commission de 6% sur le chiffre d’affaires HT du modèle « XL100 » à partir du 50e surf vendu.
- Une commission progressive sur le modèle « XS50 » :	
    - 4% sur le CA jusqu’à la 20ème planche
	- 6% du 21e au 50e surf
	- 10% à partir du 51e surf vendu



<u>**Travail à faire :**</u> Ecrire l’algorithme qui permet de calculer pour un mois quelconque la rémunération brute mensuelle d’un représentant.

## Exercice 5 - La société « Skiplus » suite !

La société « Skiplus » aimerait verser une indemnité kilométrique à ses représentants. Cette indemnité mensuelle serait de 0.15€ du km parcouru. 
Elle est limitée à 350€ mensuel.

<u>**Travail à faire :**</u> Ecrire l’extrait de l’algorithme qui permet de calculer l’indemnité kilométrique versée à un représentant.