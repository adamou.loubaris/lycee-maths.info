---
date: September 5, 2022
geometry: "left=2cm,right=2cm,top=1.3cm,bottom=1.3cm"
output: pdf_document
---
#
## Exercice 1
```
DEBUT
    AFFICHER(1*9)
    AFFICHER(2*9)
    AFFICHER(3*9)
    AFFICHER(4*9)
    AFFICHER(5*9)
    AFFICHER(6*9)
    AFFICHER(7*9)
    AFFICHER(8*9)
    AFFICHER(9*9)
    AFFICHER(10*9)
FIN
```
Écrire un algorithme équivalent en utilisant la boucle POUR...FINPOUR

## Exercice 2
