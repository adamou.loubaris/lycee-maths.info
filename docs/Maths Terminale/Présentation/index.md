---
hide:
  - footer
  - toc
---
#
L'année de terminale dans la spécialité Maths est plus facile à suivre que l'année de première, en effet les bases ont été mises en place en première.

Pour réussir en Terminale, il est nécessaire d'avoir acquis:

  - Quelques réflexes en calcul

  - un bon questionnement sur la rédaction (on rappelle qu'une bonne rédaction est une rédaction minimale et efficace), vous devez donc savoir, grâce aux activités et au cours, choisir quels arguments donner pour être certains de ne pas être pénalisés.

  - une méthode de travail basée sur la régularité

  - un minimum d'autonomie : il faut être capable de reconnaître quand on sait faire, et que l'on peut donc aller plus vite, plus loin, plus fort, mais aussi reconnaître les situations dans lesquelles il faut se remettre en question et reprendre des bases.

**L'objectif principal de l'année de terminale est de se préparer du mieux possible pour une poursuite d'études qui peut s'avérer ambitieuse en Mathématiques.**

Pour cela, vous disposez des devoirs, avec leurs corrigés, proposés ces dernières années en spécialité mathématiques.
Essayez bien sûr de chercher par vous-même avant de regarder le corrigé.

Si vous pensez avoir correctement répondu aux questions, veillez à bien vérifier que votre rédaction est pertinente et précise.

Il peut bien sûr y voir des erreurs, dans les sujets ainsi que dans les corrigés, n'hésitez pas à le signaler.
