---
hide:
  -footer
  -toc
---
#
**Vidéos pour les élèves de Terminale en spécialité Maths**

##Suites récurrence

<iframe src="https://player.vimeo.com/video/748315814?h=5c1f0a9b12" width="640" height="564" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>


<iframe src="https://player.vimeo.com/video/748318031?h=25a6febaa4" width="640" height="564" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>