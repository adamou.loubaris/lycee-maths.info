---
show:
  - navigation
hide:
  - toc
---
#
=== "`Sujet`"

    <center>

    **Petit Devoir**

    **Récurrence et Suites**

    **2019-2020**

    *Un soin particulier sera apporté à la rédaction et à la propreté de la copie.*

    *Durée:34min*

    </center>

    Soit la suite $(u_n)$ définie par $u_0=1$ , $u_1=3$ et, pour tout entier $n\geqslant 0$ : 
    $u_{n+2}=\frac 3 2u_{n+1}-\frac 1 2u_n$ .

    On pose, pour tout entier naturel $n$ , $v_n=u_{n+1}-\frac 1 2u_n$  et $w_n=5-u_n$ .

    1. Calculer $u_2$ et $u_3$ .
    2. Montrer sans démonstration par récurrence que $(v_n)$ est constante.
    3. En déduire que pour tout entier naturel $n$ : $u_{n+1}=\frac 1 2u_n+\frac 5 2$ 
    4. Montrer par récurrence que, pour tout entier naturel : $1\leqslant u_n\leqslant u_{n+1}\leqslant 5$ (cette propriété sera notée $\text P_n$ ).
    5. Que peut-on en déduire pour la suite $(u_n)$ ?
    6. Montrer que la suite $(w_n)$ est géométrique.
    7. En déduire l'expression de $u_n$ en fonction de $n$ .

=== "`Corrigé`"

    1.  $u_2=\frac 3 2u_1-\frac 1 2u_0=\frac 9 2-\frac 1 2=4$

        $u_3=\frac 3 2u_2-\frac 1 2u_1=\frac{12} 2-\frac 3 2=\frac 9 2$

    2.  $v_{n+1}=u_{n+2}-\frac 1 2u_{n+1}=\frac 3 2u_{n+1}-\frac 1 2u_n-\frac 1 2u_{n+1}=u_{n+1}-\frac 1 2u_n=v_n$
        
        $(v_n)$ est donc bien une suite constante.

    3. $v_0=u_1-\frac 1 2u_0=\frac 5 2$ et $(v_n)$ constante donc $v_n=\frac 5 2$ pour tout $n \in ℕ$ ,

        Avec la définition de $(v_n)$, on a donc $\frac 5 2=u_{n+1}-\frac 1 2u_n$ soit $u_{n+1}=\frac 1 2u_n+\frac 5 2$ . 

    4. Soit : $\text P_n$ :  $\text{«}1\leqslant u_n\leqslant u_{n+1}\leqslant 5\text{»}$ .

        <u>*Initialisation*</u>

        On a $u_0=1$ et $u_1=3$ donc : $\text P_0$ :  $\text{«}1\leqslant u_0\leqslant u_1 \leqslant5 \text{»}$ est vraie.

        La propriété est bien initialisée.
        
        <u>*Hérédité*</u>

        Supposons $\text P_n$ vraie pour un rang  donné. Montrons qu'alors $\text P_{n+1}$ est vraie.

        $1\leqslant u_n\leqslant u_{n+1}\leqslant 5$ ⇒ $\frac 1 2\times 1\leqslant \frac 1 2\times u_n\leqslant \frac 1 2\times u_{n+1}\leqslant \frac 1 2\times 5$

        ⇒ $\frac 1 2+\frac 5 2\leqslant \frac 1 2u_n+\frac 5 2\leqslant \frac 1 2u_{n+1}+\frac 5 2\leqslant \frac 5 2+\frac 5 2$

        ⇒ $3\leqslant u_{n+1}\leqslant u_{n+2}\leqslant 5$ 
        
        ⇒ $1\leqslant u_{n+1}\leqslant u_{n+2}\leqslant 5$ 
        
        ⇒ $\text P_{n+1}$ vraie, l'hérédité est vérifiée.
        
        <u>*Conclusion*</u>
        
        Par récurrence sur $n$, on a donc montré que $\text P_{n}$ était vraie pour tout $n \in ℕ$.

    5. La suite $(u_n)$ est donc croissante, minorée par 1 et majorée par 5.
            
    6. $w_{n+1}=5-u_{n+1}=5-\left(\frac 1 2u_n+\frac 5 2\right)=\frac 5 2-\frac 1 2u_n=\frac 1 2(5-u_n)=\frac 1 2w_n$

        La suite $(w_n)$ est donc bien géométrique de raison $\frac 1 2$ et de premier terme $w_0=5-u_0=5-1=4$ .
        
    7. La forme explicite de $(w_n)$ est $w_n=4\times \left(\frac 1 2\right)^n$ et $w_n=5-u_n$ ⇒ $u_n=5-w_n$ .

        La forme explicite de $(u_n)$ est $u_n=5-4\left(\frac 1 2\right)^n$