---
hide:
  - footer
  - toc
---
L'année de première en NSI est une découverte et/ou un approfondissement du langage Python mais aussi un apprentissage des méthodes nécessaires pour résoudre des problèmes informatiques.

Comme dans l'apprentissage d'une langue, la pratique régulière est importante pour acquérir des automatismes.

La plupart du temps, nous travaillons sur des notebooks jupyter (l'outil idoine sur classroom est colaboratory), la découverte du fonctionnement de ces notebooks est faite lors du premier cours.

Ces pages mettent à disposition les versions statiques des cours, mais aussi des devoirs ou encore des propositions de projets qui peuvent permettre aux élèves de progresser.

on essaiera, dans la mesure du possible, de fournir des corrigés.

Il peut bien sûr y voir des bugs ou des coquilles dans les cours ainsi que dans les corrigés, n'hésitez pas à le signaler.
