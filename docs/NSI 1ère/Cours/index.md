---
hide:
  -footer
  -toc
---

#**Cours pour les élèves de première en spécialité NSI**

Les versions `Jupyter Notebook` sont distribuées en classe, sur classroom.

## Bases de Python

&nbsp;&nbsp;&nbsp;[1 - Variables](variables.md)

&nbsp;&nbsp;&nbsp;[2 - Conditions](conditions.md)

&nbsp;&nbsp;&nbsp;[3 - Boucles](boucles.md)

&nbsp;&nbsp;&nbsp;[4 - Fonctions](fonctions.md)

## Types des variables

&nbsp;&nbsp;&nbsp;[1 - Entiers](entiers.md)